# Test textile syntax

## Code block

```
#!perl
my $word = "we'll not carry coals";
my $test = $word . " anymore";
for my $i (1..10) {
	print $i, "\n";
}
```

```
#!bash
cp -a test{.bak,$ext}
```

```
my $a = 1;

my $b = 2;
```

Continuing text


## Definition lists

* One := This is one of a kind
* Two := This is two of a kind


## Ordered and unordered lists

1. One
1. Two
	1. Two and a half
	1. Two without a knob on
1. Three

* This is getting boring
* I need to do something else
	* Code
	* Beer
* Go home

You can find more information at {this nice website}(http://www.google.com).



